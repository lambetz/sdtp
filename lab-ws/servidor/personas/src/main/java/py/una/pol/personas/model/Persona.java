package py.una.pol.personas.model;

import py.una.pol.personas.dao.*;
import java.io.Serializable;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;



import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;

import java.util.List;

import javax.ws.rs.core.Response;
//import java.util.ArrayList;
//import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Persona  implements Serializable {

	public static int index;
	
	@Inject
	private Logger log;
	
	Long cedula;
	String nombre;
	String apellido;
	int cantdeasignaturas;
	String [] asignaturas = new String[cantdeasignaturas];
	
//	String asignatura;
//	Asignatura asignatura = new Asignatura();

//	ArrayList<Asignatura> asignaturas = new ArrayList<Asignatura>();
	
//	public Persona(){
//		asignaturas = new ArrayList<String>();
//	}

	public Persona(Long pcedula, String pnombre, String papellido, Integer pcantdeasignaturas, String[] pasignaturas){
		this.cedula = pcedula;
		this.nombre = pnombre;
		this.apellido = papellido;
		this.cantdeasignaturas = pcantdeasignaturas;
		this.asignaturas = pasignaturas;
		
//		asignaturas.add(pasignatura);
		
//		this.asignatura = pasignatura;
		

//		Asignatura asignatura = new Asignatura();
//		asignatura = pasignatura;
		

//		asignaturas = new ArrayList<Asignatura>();
	}
	

	public Persona() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getCantdeasignaturas() {
		return cantdeasignaturas;
	}


	public void setCantdeasignaturas(int cantdeasignaturas) {
		this.cantdeasignaturas = cantdeasignaturas;
	}


	public String[] getAsignaturas() {
		return asignaturas;
	}


	public void setAsignaturas(String[] asignaturas) {
		
//		System.out.println("###################################################################################");
		
//	   	 for (int i=0; i<cantdeasignaturas; i++) {
//			 System.out.println( "\n^^^^^^^\n" + asignaturas[i]);
//		 }
		
		this.asignaturas = asignaturas;
	}
	

	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}




//	public List<String> getAsignaturas() {
//		return asignaturas;
//	}

//	public void setAsignaturas(List<String> asignaturas) {
//		this.asignaturas = asignaturas;
//	}
	
//	public String getAsignatura() {
//		return asignatura.getNombre();
//	}
//
//	public void setAsignatura(String asignatura) {
//		this.asignatura.setNombre(asignatura);
//		
//	}
//	


}
