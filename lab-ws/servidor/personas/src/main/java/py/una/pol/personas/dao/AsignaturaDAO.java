package py.una.pol.personas.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

@Stateless
public class AsignaturaDAO {

    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	public List<Asignatura> seleccionar() throws Exception {

		String query = "SELECT nombre, profesor, maxalumnos, alumnos FROM asignatura ";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Asignatura a = new Asignatura();
        		a.setNombre(rs.getString(1));
        		a.setProfesor(rs.getString(2));
        		a.setMaxalumnos(rs.getInt(3));
        		
	           	Array alumnos = rs.getArray(4);
	           	String[] str_alumnos = (String[])alumnos.getArray();
	           	a.setAlumnos(str_alumnos);
	           	
	           	for (int i=0; i<a.getMaxalumnos(); i++) {
	           		System.out.println("\n^^^^^^^^^^^^^^^^^^^^\n" + str_alumnos[i]);
	           		 
	           	}
	        		
	        	lista.add(a);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public Asignatura seleccionarPorNombre(String nombre) {

		String SQL = "SELECT nombre, profesor, maxalumnos, alumnos FROM asignatura WHERE nombre = ? ";
		
		Asignatura a = null;
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1, nombre);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		a = new Asignatura();
        		a.setNombre(rs.getString(1));
        		a.setProfesor(rs.getString(2));
        		a.setMaxalumnos(rs.getInt(3));
        		
	           	Array alumnos = rs.getArray(4);
	           	String[] str_alumnos = (String[])alumnos.getArray();
	           	a.setAlumnos(str_alumnos);
//	           	
//	           	for (int i=0; i<a.getMaxalumnos(); i++) {
//	           		System.out.println("\n^^^^^^^^^^^^^^^^^^^^\n" + str_alumnos[i]);
//	           		 
//	           	}
        		
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return a;

	}
	
	
    public String insertar(Asignatura a) throws SQLException {

        String SQL = "INSERT INTO asignatura(nombre,profesor,maxalumnos,alumnos) "
                + "VALUES(?,?,?,?)";
 
        String id = "";
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getNombre());
            pstmt.setString(2, a.getProfesor());
            pstmt.setInt(3, a.getMaxalumnos());
            
	   	   	String[] alumn = new String[a.getMaxalumnos()];
	        alumn = a.getAlumnos();
	        
            Array arrayAlumnos = conn.createArrayOf("text", alumn);
            pstmt.setArray(4, arrayAlumnos);

			int count = 0;
			for(String i : alumn) {
			    if(i != null) {
			        count++;
			    }
			}
			
			System.out.println("\n##############\n");
			System.out.println("\n"+ count +"\n");
//            int cuenta=0;
//           	for (int i=0; i<(a.getMaxalumnos()-1); i++) {
//         		System.out.println("^^^^^^^^^" + alumn[i]);
//           		if (alumn[i]!=null) {
//           			cuenta++;
//           		}
//           		 
//           	}
           	
//           	System.out.println("\nCANTIDAD DE ALUMNOS INSCRIPTOS\n");
//           	System.out.println(cuenta);
            
//            System.out.println("\n##################################\n");
//            System.out.println(alumn.length);
            
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getString(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    	
    }
	

    public String actualizar(Asignatura a) throws SQLException {
    	
        String SQL = "UPDATE asignatura SET profesor = ?, maxalumnos = ?, alumnos = ? WHERE nombre = ? ";
 
        String id = "";
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getProfesor());
            pstmt.setInt(2, a.getMaxalumnos());
            pstmt.setString(4, a.getNombre());
            
	   	   	String[] alumn = new String[a.getMaxalumnos()];
	        alumn = a.getAlumnos();
	        
            Array arrayAlumnos = conn.createArrayOf("text", alumn);
            pstmt.setArray(3, arrayAlumnos);
            
           	for (int i=0; i<a.getMaxalumnos(); i++) {
           		System.out.println("\n^^^^^^^^^^^^^^^^^^^^\n" + alumn[i]);
           		 
           	}
           	
            //if(existeAsignatura(a.getNombre())) {
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getString(1);
	                    }
	                } catch (SQLException ex) {
	                    System.out.println(ex.getMessage());
	                }
	            }
            //}
	        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    
    
    public String actualizarasignatura(Asignatura a, String nombrepersona) throws SQLException {
    	
  //  	System.out.println("\n########\n" + a.getNombre() + " " + a.getProfesor() + "\n");
  //  	System.out.println("\nNOMBREEEEEEE\n" + nombrepersona);
    	
        String SQL = "UPDATE asignatura SET profesor = ?, maxalumnos = ?, alumnos = ? WHERE nombre = ? ";
 
        String id = "";
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getProfesor());

            pstmt.setInt(2, a.getMaxalumnos());
            pstmt.setString(4, a.getNombre());
            
	   	   	String[] alumn = new String[a.getMaxalumnos()];
	        alumn = a.getAlumnos();
	        
			int count = 0;
			for(String i : alumn) {
			    if(i != null) {
			        count++;
			    }
			}
			
//			System.out.println("\n##############\n");
//			System.out.println("\n"+ count +"\n");

            int indice = count + 1;
	        
//            System.out.println("\n############\n" + indice + "\n");
            
	   	   	String[] alumno = new String[a.getMaxalumnos()];
	        alumno = a.getAlumnos();            

//	        System.out.println("############" + alumno[0]);
	        
//	        String value = String.valueOf(nombrepersona);
	        
//	        alumno[indice] = nombrepersona;
	        
            Array arrayAlumnos = conn.createArrayOf("text", alumn);
            pstmt.setArray(3, arrayAlumnos);
           	
            //if(existeAsignatura(a.getNombre())) {
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getString(1);
	                    }
	                } catch (SQLException ex) {
	                    System.out.println(ex.getMessage());
	                }
	            }
            //}
	        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public String borrar(String nombre) throws SQLException {

        String SQL = "DELETE FROM asignatura WHERE nombre = ? ";
 
        String id = "";
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setString(1, nombre);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getString(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    
	@SuppressWarnings("unused")
	public Boolean existeAsignatura(String nombre) {
		Asignatura asignatur = new Asignatura();
		AsignaturaDAO adao = new AsignaturaDAO();
		//asignatur.nombre = "";
		asignatur = adao.seleccionarPorNombre(nombre);
		System.out.println("\n########################\n" + asignatur.getNombre());
		//log.info(asignatur.nombre);
		
		Boolean resp;
		if (asignatur==null)
			resp=false;
		else
			resp=true;
		
		return resp;
		//return asignatur.nombre;
		
	}

    

}
