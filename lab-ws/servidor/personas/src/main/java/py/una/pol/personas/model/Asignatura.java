package py.una.pol.personas.model;

import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura  implements Serializable {

	String nombre;
	String profesor;
	int maxalumnos;

	String [] alumnos = new String[maxalumnos];

	public Asignatura(String anombre, String aprofesor, int amaxalumnos, String[] aalumnos){
		this.nombre = anombre;
		this.profesor = aprofesor;
		this.maxalumnos = amaxalumnos;
		this.alumnos = aalumnos;
	}

	public Asignatura() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public int getMaxalumnos() {
		return maxalumnos;
	}

	public void setMaxalumnos(int maxalumnos) {
		this.maxalumnos = maxalumnos;
	}

	public String[] getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(String[] alumnos) {
		this.alumnos = alumnos;
	}


}
