package py.una.pol.personas.dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;

@Stateless
public class PersonaDAO {
 
	
    @Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 * @throws Exception 
	 */
	public List<Persona> seleccionar() throws Exception {

		String query = "SELECT cedula, nombre, apellido, cantdeasignaturas, asignaturas FROM persona ";
		
		List<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		p.setCantdeasignaturas(rs.getInt(4));
        		                
            	Array asignaturas = rs.getArray(5);
            	String[] str_asignaturas = (String[])asignaturas.getArray();
            	p.setAsignaturas(str_asignaturas);
            	
            	for (int i=0; i<p.getCantdeasignaturas(); i++) {
            		System.out.println("\n^^^^^^^^^^^^^^^^^^^^\n" + str_asignaturas[i]);
            		 
            	}
            	 
//                p.setAsignaturas(rs.getArray(5));
        		
                
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public Persona seleccionarPorCedula(long cedula) throws Exception {

		String SQL = "SELECT cedula, nombre, apellido, cantdeasignaturas, asignaturas FROM persona WHERE cedula = ? ";
		
		Persona p = null;
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));
        		p.setCantdeasignaturas(rs.getInt(4));
        		
	           	Array asignaturas = rs.getArray(5);
	           	String[] str_asignaturas = (String[])asignaturas.getArray();
	           	p.setAsignaturas(str_asignaturas);
	           	
	           	for (int i=0; i<p.getCantdeasignaturas(); i++) {
	           		System.out.println("\n^^^^^^^^^^^^^^^^^^^^\n" + str_asignaturas[i]);
	           		 
	           	}
        		//p.setAsignatura(rs.getString(4));
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return p;

	}
	
	
    public long insertar(Persona p) throws SQLException {

        String SQL = "INSERT INTO persona(cedula,nombre,apellido,cantdeasignaturas,asignaturas) "
                + "VALUES(?,?,?,?,?)";
 
        long id = 0;
        Connection conn = null;

        Boolean existeAsignatura = false;
        
	   	Asignatura a = new Asignatura();
	   	AsignaturaDAO adao = new AsignaturaDAO();

        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, p.getCedula());
            pstmt.setString(2, p.getNombre());
            pstmt.setString(3, p.getApellido());
            pstmt.setInt(4,  p.getCantdeasignaturas());
            
//			int count = 0;
//			for(String i : alumn) {
//			    if(i != null) {
//			        count++;
//			    }
//			}


   	   		for (int i=0; i<p.getCantdeasignaturas(); i++) {
   	   	   		String[] asigna = new String[p.getCantdeasignaturas()];
   	   	   		asigna = p.getAsignaturas();
   	   			
   	   			//System.out.println("\n^^^^^^^^^^^\n" + i + " " + asigna[i]);
	   	   		
	   	   		
	   	   		a = adao.seleccionarPorNombre(asigna[i]);	   	
	   	   		
	   	   		
	   	   		if(a.getNombre()!=null) {
	   	   			existeAsignatura=true;
	   	   		}
	   	   		
	   	   		//System.out.println("\n^^^^^^^^^^\n" + a.getNombre() + " " + a.getProfesor() + "\n^^^^^^^^^^^^^^\n");
	   	   		
	   	   			   	   		
	   	   		String hola = adao.actualizarasignatura(a,p.getNombre());

//	   	   		System.out.println("\n##########\n" + hola);
	   	   		
//	   			System.out.println( "\n^^^^^^^\n" + asigna[i]);
	   			
//	   			pstmt.setString(5, asigna[i]);
	   		}
	        
	   	   	String[] asignatur = new String[p.getCantdeasignaturas()];
	        asignatur = p.getAsignaturas();
	        
            Array arrayAsignaturas = conn.createArrayOf("text", asignatur);
            pstmt.setArray(5, arrayAsignaturas);
            
//            if(existeAsignatura(p.getAsignatura())) {

            if(existeAsignatura) {
	            int affectedRows = pstmt.executeUpdate();
		        // check the affected rows 
		        if (affectedRows > 0) {
		        	// get the ID back
		        	try (ResultSet rs = pstmt.getGeneratedKeys()) {
		        		if (rs.next()) {
		        			id = rs.getLong(1);
		        		}
		        	} catch (SQLException ex) {
		        		throw ex;
		            }
		        }
            }
		//}
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
 
        return id;	
    	
    }
	

    public long actualizar(Persona p) throws SQLException {
  
        String SQL = "UPDATE persona SET nombre = ? , apellido = ?, cantdeasignaturas = ?, asignaturas = ? WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(5, p.getCedula());
        	pstmt.setString(1, p.getNombre());
            pstmt.setString(2, p.getApellido());
            pstmt.setInt(3, p.getCantdeasignaturas());
            
//            System.out.println("\n######APELLIDOOOOOOO:\n" + p.getApellido());
            
            
	   	   	String[] asignatur = new String[p.getCantdeasignaturas()];
	        asignatur = p.getAsignaturas();
	        
//	   	   	for (int i=0; i<p.getCantdeasignaturas(); i++) {
//	   	   		String[] asigna = new String[p.getCantdeasignaturas()];
//	   	   		asigna= p.getAsignaturas();
//	   			System.out.println( "\n^^^^^^^\n" + asigna[i]);
//	   			pstmt.setString(5, asigna[i]);
//	   	   	}
//	        
	        
//	        System.out.println("\n######111111111111111111111\n");
	        
            Array arrayAsignaturas = conn.createArrayOf("text", asignatur);
            pstmt.setArray(4, arrayAsignaturas);
            
            //pstmt.setString(4, p.getAsignatura());
 
            
//	        System.out.println("\n######2222222222222222222\n");
            
            
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            
//	        System.out.println("\n######333333333333333333333333333\n");
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                        
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
    
    public long borrar(long cedula) throws SQLException {

        String SQL = "DELETE FROM persona WHERE cedula = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, cedula);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    
    
	@SuppressWarnings("unused")
	public Boolean existeAsignatura(String nombre) {
		Asignatura asignatur = new Asignatura();
		AsignaturaDAO adao = new AsignaturaDAO();
		//asignatur.nombre = "";
		asignatur = adao.seleccionarPorNombre(nombre);
		System.out.println("\n########################\n" + asignatur.getNombre());
		//log.info(asignatur.nombre);
		
		Boolean resp;
		if (asignatur==null)
			resp=false;
		else
			resp=true;
		
		return resp;
		//return asignatur.nombre;
		
	}

}
